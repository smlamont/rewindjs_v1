const gulp = require('gulp');
const rewindJS = require('./node_modules/@rewindJS_components/rewindJS/gulp_helper');
const pkg = require('./package.json');

rewindJS.createTasks(gulp, {
  pkg,
  embedArea: 'left',
  environmentOverride: null,
  deploymentPath: '',
  preprocessorContext: {
    local: {},
    dev: {},
    qa: {},
    prod: {}
  }
});
