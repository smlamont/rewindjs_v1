my_app_name
===========
Describe your app.

For embedded apps:
------------------
This app can be embedded on a Wordpress page with:

`[rew_app app="my_app_name"][/rew_app]` or by using the insert application button in the tinyMCE editor.

