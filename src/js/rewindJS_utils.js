/**
 These are useful utility functions
 */

class rewindJSUtils {

  static getHashValues() {
    let hashVals = window.location.hash.substr(1);
    let hashObj = {};
    let search = /(.*?)\/(.*?)\//g;
    let match;
    while (match = search.exec(hashVals)) {
      hashObj[match[1]] = decodeURIComponent(match[2]);
    }
    return hashObj;
  }
   static extendAndCleanHashValues(hash, hObj) {
	let newHashObj = $.extend({}, hash, hObj);
	$.each(Object.keys(newHashObj), (i, key) => {
		if (!newHashObj[key]) {
				delete newHashObj[key];
		}
	})
    return newHashObj;
  }
  static putHashValues(hash) {
	  window.location.hash = "#" + rewindJSUtils.getHashValueStr(hash);
  }
  static getHashValueStr(hash) {
    let hashStr = "";
    $.each(Object.keys(hash), (i, key) => {
      if (hash[key]) hashStr += key + "/" + hash[key] + "/";
    });
    return hashStr;
  }

  //Sort an array of jason objects. Use +- for asc/desc. Can sort on multiple properties
  //ex: this.data_feed.sort(NewroomApp.sortJSONData'-spotlight', '-publishDate'));

  static sortJSONData() {
    let _args = Array.prototype.slice.call(arguments);
    return function (a, b) {
      for (let x = 0; x < _args.length; x++) {
        let ax = a[_args[x].substring(1)];
        let bx = b[_args[x].substring(1)];
        let cx;

        ax = typeof ax === "string" ? ax.toLowerCase() : ax / 1;
        bx = typeof bx === "string" ? bx.toLowerCase() : bx / 1;

        if (_args[x].substring(0, 1) === "-") {
          cx = ax;
          ax = bx;
          bx = cx;
        }
        if (ax !== bx) {
          return ax < bx ? -1 : 1;
        }
      }
    }
  }
}
