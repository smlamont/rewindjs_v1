 |Libraries |Standalone Apps |Embedded Apps |
 |----------|----------------|--------------|
 |[ jquery 2.2.4 ](http://jquery.com/) | ​Included via injection | Included via website |
 |[ ​bootstrap 3.3.7 ](https://getbootstrap.com/docs/3.3/css/) | ​Included via injection | Included via website |
 | html5shiv, respond.js  | ​Included via injection | Included via website |
 | roboto fonts  | ​Included via injection | Included via website |
 | rewindJS_app, bootstrap_rewindJS_customizations  | ​Included via injection | Not available |
 | embedded_rewindJS_app  | ​Included via injection | Not available |
 |[ jquery.cookie.js ](https://github.com/carhartl/jquery-cookie) | ​Included via injection | |
 |[ backbone ](http://backbonejs.org) [ underscore ](http://underscorejs.org/), rewindJS_backbone| Optional injection via rewindJSConfig.​includeModeling​ | |
 |[ handlebars ](https://handlebarsjs.com/) | Optional injection via rewindJSConfig.​includeTemplates​ | |
 |[ ​formValidation ](http://formvalidation.io/), rewindJS_forms | ​Optional injection via rewindJSConfig.includeFormValidation | |
 |[ jintlTelInput ](https://github.com/jackocnr/intl-tel-input) | ​Optional injection via rewindJSConfig.includeIntlTelInput | |
 |[ jquery.maskedinput ](http://digitalbush.com/projects/masked-input-plugin/) | ​Optional injection via rewindJSConfig.includeJQueryMaskedInput | |
 |[ jquery-editable-select ](http://indrimuska.github.io/jquery-editable-select/) | ​Optional injection via rewindJSConfig.includeEditableSelect | |
 | ​placeholders.jquery | Optional injection via rewindJSConfig.includePlaceholders| |
 |[ bootstrap-multiselect ](http://davidstutz.github.io/bootstrap-multiselect/) | ​Optional injection via rewindJSConfig.includeMultiSelect | |
 |[ OMS for google maps ](https://github.com/jawj/OverlappingMarkerSpiderfier) | Optional injection via rewindJSConfig.includeOMS | |
 |[ moment.js ](http://momentjs.com/) |  Included via injection if any of the following are true:| |
 || 		rewindJSConfig.includeMoment| |
 ||         rewindJSConfig.includeFullCalendar| |
 ||         rewindJSConfig.includeDatePicker| |
 ||         rewindJSConfig.includeRangePicker| |
 | fullcalendar | Optional injection via rewindJSConfig.includeFullCalendar | |
 |[ bootstrap-datetimepicker ](http://eonasdan.github.io/bootstrap-datetimepicker/) | Optional injection via rewindJSConfig.includeDatePicker | |
 |[ daterangepicker ](http://www.daterangepicker.com/) | Optional injection via rewindJSConfig.includeRangePicker | |
 |[ bootbox](http://bootboxjs.com/) | Optional injection via rewindJSConfig.includeBootbox | |
 |[ dropzone ](http://www.dropzonejs.com/) | Optional injection via rewindJSConfig.includeDropzone | |
 | rewindJS_modal | Optional injection via rewindJSConfig.includeModal | |
 | rewindJS_terms | Optional injection via rewindJSConfig.includeTerms | |
 |​​embedded_rewindJS_forms (CSS) | Optional injection via rewindJSConfig.includeFormValidation | |
 
