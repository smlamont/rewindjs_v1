'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

//extending base backbone classes with some helpers
var rewindJSModel = Backbone.Model.extend({
    get: function get(key) {
        var keys = (key || '').split('.');
        var value = Backbone.Model.prototype.get.apply(this, [keys[0]]);
        if (keys.length == 1) {
            return value;
        }
        if ((typeof value === 'undefined' ? 'undefined' : _typeof(value)) !== 'object') {
            return undefined;
        }
        for (var i = 1; i < keys.length && value != undefined; i++) {
            value = value[keys[i]];
        }
        return value;
    },
    set: function set(key, val, options) {
        if ((typeof key === 'undefined' ? 'undefined' : _typeof(key)) == 'object') {
            for (var k in key) {
                if (key.hasOwnProperty(k)) {
                    this._set(k, key[k], val);
                }
            }
        } else {
            this._set(key, val, options);
        }
        return this;
    },
    _set: function _set(key, val, options) {
        var keys = (key || '').split('.');
        if (keys.length == 1) {
            var object = this.get(key);
            if (object && object instanceof rewindJSCollection && !(val instanceof rewindJSCollection)) {
                object.set(val);
            } else {
                Backbone.Model.prototype.set.apply(this, arguments);
            }
        } else {
            var object = this.get(keys[0]);
            if ((typeof object === 'undefined' ? 'undefined' : _typeof(object)) !== 'object') {
                object = {};
            }{
                object = _.clone(object);
            }
            var lastObject = object;
            for (var i = 1; i < keys.length - 1; i++) {
                if (lastObject[keys[i]] === undefined) {
                    lastObject[keys[i]] = {};
                }
                lastObject = lastObject[keys[i]];
            }
            var lastKey = keys[keys.length - 1];
            var oldValue = lastObject[lastKey];
            lastObject[lastKey] = val;
            Backbone.Model.prototype.set.apply(this, [keys[0], object]);
        }
    },
    toJSON: function toJSON() {
        var json = Backbone.Model.prototype.toJSON.apply(this);
        for (var k in json) {
            if (json[k] && json[k].toJSON) {
                json[k] = json[k].toJSON();
            }
        }
        return json;
    }
});
var rewindJSView = Backbone.View.extend({
    localId: function localId(s) {
        return (this.id || this._fallbackId()) + '_' + s;
    },
    localEl: function localEl(s) {
        return this.$('#' + this.localId(s));
    },
    _fallbackId: function _fallbackId() {
        this._fallbackId_val = this._fallbackId_val || Math.random().toString().split('.')[1];
        return this._fallbackId_val;
    }
});
var rewindJSCollection = Backbone.Collection.extend({ model: rewindJSModel });