"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 These are useful utility functions
 */

var rewindJSUtils = function () {
  function rewindJSUtils() {
    _classCallCheck(this, rewindJSUtils);
  }

  _createClass(rewindJSUtils, null, [{
    key: "getHashValues",
    value: function getHashValues() {
      var hashVals = window.location.hash.substr(1);
      var hashObj = {};
      var search = /(.*?)\/(.*?)\//g;
      var match = void 0;
      while (match = search.exec(hashVals)) {
        hashObj[match[1]] = decodeURIComponent(match[2]);
      }
      return hashObj;
    }
  }, {
    key: "extendAndCleanHashValues",
    value: function extendAndCleanHashValues(hash, hObj) {
      var newHashObj = $.extend({}, hash, hObj);
      $.each(Object.keys(newHashObj), function (i, key) {
        if (!newHashObj[key]) {
          delete newHashObj[key];
        }
      });
      return newHashObj;
    }
  }, {
    key: "putHashValues",
    value: function putHashValues(hash) {
      window.location.hash = "#" + rewindJSUtils.getHashValueStr(hash);
    }
  }, {
    key: "getHashValueStr",
    value: function getHashValueStr(hash) {
      var hashStr = "";
      $.each(Object.keys(hash), function (i, key) {
        if (hash[key]) hashStr += key + "/" + hash[key] + "/";
      });
      return hashStr;
    }

    //Sort an array of jason objects. Use +- for asc/desc. Can sort on multiple properties
    //ex: this.data_feed.sort(NewroomApp.sortJSONData'-spotlight', '-publishDate'));

  }, {
    key: "sortJSONData",
    value: function sortJSONData() {
      var _args = Array.prototype.slice.call(arguments);
      return function (a, b) {
        for (var x = 0; x < _args.length; x++) {
          var ax = a[_args[x].substring(1)];
          var bx = b[_args[x].substring(1)];
          var cx = void 0;

          ax = typeof ax === "string" ? ax.toLowerCase() : ax / 1;
          bx = typeof bx === "string" ? bx.toLowerCase() : bx / 1;

          if (_args[x].substring(0, 1) === "-") {
            cx = ax;
            ax = bx;
            bx = cx;
          }
          if (ax !== bx) {
            return ax < bx ? -1 : 1;
          }
        }
      };
    }
  }]);

  return rewindJSUtils;
}();