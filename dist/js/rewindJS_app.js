'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var rewindJS_app = function () {
  function rewindJS_app(sName, options) {
    _classCallCheck(this, rewindJS_app);

    this.name = sName;
    //configurable options:
    $.extend(this, {
      hasHeader: true, //set to false to hide the main corporate header portion of the app interface
      hasFooter: true, //set to false to hide the main corporate footer portion of the app interface
      hasContentTop: true, //set to false if you don't need this portion of the content area
      hasContentBottom: true, //set to false if you don't need this portion of the content area
      hasContentRight: true, //set to false if you don't need this portion of the content area
      hasContentLeft: true, //set to false if you don't need this portion of the content area
      hasLeftNav: false, //set to true if you want to have a left hand navigation area
      searchcontext: 'INTER' //set the search context of this app, allowable values: 'INTER', 'INTRA', '311'
    }, options || {});

    this.breadcrumbItems = [];
    this.isRendered = false;
  }

  _createClass(rewindJS_app, [{
    key: 'rewindJS_app',
    value: function rewindJS_app(sName, options) {
      this.name = sName;
      //configurable options:
      $.extend(this, {
        hasHeader: true, //set to false to hide the main corporate header portion of the app interface
        hasFooter: true, //set to false to hide the main corporate footer portion of the app interface
        hasContentTop: true, //set to false if you don't need this portion of the content area
        hasContentBottom: true, //set to false if you don't need this portion of the content area
        hasContentRight: true, //set to false if you don't need this portion of the content area
        hasContentLeft: true, //set to false if you don't need this portion of the content area
        hasLeftNav: false, //set to true if you want to have a left hand navigation area
        searchcontext: 'INTER' //set the search context of this app, allowable values: 'INTER', 'INTRA'
      }, options || {});

      this.breadcrumbItems = [];
      this.isRendered = false;
    }
  }, {
    key: 'setTitle',
    value: function setTitle(title) {
      $("#app-header").find("h1").html(title);
    }
  }, {
    key: 'setBreadcrumb',
    value: function setBreadcrumb(items, excludeAppName) {
      //items: an array of raw javascript objects that define breadcrumb items, supported properties are:
      //link: if specified, the breadcrumb item will be a link to this URL
      //      NOTE: the last item is never a link
      //name: required, the title of the breadcrumb item
      //excludeAppName: By default, the app name is automatically set as the last breadcrumb item. Set this to true if you do not want that to happen

      this.breadcrumbItems = items;
      if (!excludeAppName) {
        this.breadcrumbItems.push({ name: this.name });
      }
      this._renderBreadcrumb();
      return this;
    }
  }, {
    key: '_renderBreadcrumb',
    value: function _renderBreadcrumb() {
      var container = $("#app-breadcrumb");
      if (container.length) {
        if (this.breadcrumbItems.length) {
          var rootUrl = this.searchcontext === 'INTER' ? 'https://www.mississauga.ca' : 'http://inside.mississauga.ca';
          var rootTitle = this.searchcontext === 'INTER' ? 'City of Mississauga' : 'InsideCOM';
          var itemsHtml = '<li><a href="' + rootUrl + '"><div class="glyphicon glyphicon-home" style="margin-right: 5px;"></div>' + rootTitle + '</a></li>';
          var lastIndex = this.breadcrumbItems.length - 1;
          $.each(this.breadcrumbItems, function (i, item) {
            if (item.name) {
              var link = item.link && i < lastIndex ? '<a href="' + item.link + '">' + item.name + '</a>' : item.name;
              itemsHtml += '<li>' + link + '</li>';
            }
          });
          container.find('ul').html(itemsHtml);
          container.show();
        } else {
          container.hide();
        }
      }
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.isRendered) {
        throw new Error('App is already rendered');
      }

      var app = this;
      app.setTitle(app.name);
      app._renderBreadcrumb();

      //SML: this needs to be updated.
      //SET UP SEARCH WITH THE PROPER INTER CONTEXT
      switch (app.searchcontext) {
        case "INTRA":
          $("#siteSearchGSA").attr("action", "http://inside-search.mississauga.ca/search");
          $("#siteSearchGSA #q").attr("placeholder", "Search Inside Mississauga...");
          break;

        default:
          $("#siteSearchGSA").attr("action", "https://find.mississauga.ca/searchblox/servlet/SearchServlet");
          $("#siteSearchGSA #q").attr("placeholder", "");
          $("#siteSearchGSA #q").attr("name", "query");

      }

      if (app.hasLeftNav) {
        $("#app-nav-left").removeClass("hide");
        $("#app-content-full").addClass("col-sm-9");
      } else {
        $("#app-nav-left").remove();
      }
      if (!app.hasHeader) {
        $("#rewindJS-header").remove();
        $("#app-header").remove();
      }
      if (!app.hasFooter) {
        $("#app-footer").remove();
      }
      if (!app.hasContentTop) {
        $("#app-content-top").remove();
      }
      if (!app.hasContentRight) {
        $("#app-content-left").removeClass('col-md-8');
        $("#app-content-right").remove();
      }
      if (!app.hasContentLeft) {
        $("#app-content-left").remove();
      }
      if (!app.hasContentBottom) {
        $("#app-content-bottom").remove();
      }
      //SML -- remvoe customizzations applyFontSize();
      $("#appDisplay").removeClass("hide");
      app.isRendered = true;
      return this;
    }

    /*
    A convenience method for writing HTML into the app content area.
    options: a javascript object with one or more of the keys 'top', 'right', 'bottom', 'left', 'nav' set to an HTML string to insert into that content area
     ex: {top: '<div>some html for the top area</div>'}
     */

  }, {
    key: 'setContent',
    value: function setContent(options) {
      var app = this;
      $.each(['top', 'right', 'bottom', 'left', 'nav'], function (i, value) {
        if (options[value] !== undefined) {
          $(app.getContentContainerSelector(value)).html(options[value]);
        }
      });
    }
  }, {
    key: 'addForm',


    /*
    A convenience method for inserting a rewindJS_form object into the app content area.
    rewindJSForm: a rewindJSForm object
     area: a string with one of five possible values: 'top', 'right', 'bottom', 'left', 'nav'
    replaceCurrentContent: defaults to true, specify false if the form should be appended into the content instead of replacing it
     */
    value: function addForm(rewindJSForm, area, replaceCurrentContent) {
      replaceCurrentContent = replaceCurrentContent === undefined ? true : replaceCurrentContent;

      if (replaceCurrentContent) {
        var clear = {};
        clear[area] = '';
        this.setContent(clear);
      }
      rewindJSForm.render({
        target: this.getContentContainerSelector(area)
      });
    }

    /*
    A convenience method for getting the jquery selector of a given app content area
    area: a string with one of five possible values: 'top', 'right', 'bottom', 'left', 'nav'
     */

  }, {
    key: 'getContentContainerSelector',
    value: function getContentContainerSelector(area) {
      return area === 'nav' ? '#app-nav-left' : '#app-content-' + area;
    }
  }]);

  return rewindJS_app;
}();