# rewindJS
[![Current Version](https://img.shields.io/badge/version-1.1.11-green.svg)](https://bitbucket.org/smlamont/rewindJS)
![node v7](https://img.shields.io/npm/v/@cycle/core.svg)

The rewindJS library for creating City of Mississauga web applications.


Getting Started
===============
1. Make sure that [npm](https://nodejs.org/en) is installed on your computer. It is best that you have the latest version installed. Otherwise you may run into issues below.
2. Make sure that [git](https://git-scm.com/downloads) is installed on your computer. It is best that you have the latest version installed. Otherwise you may run into issues below.
3. Make sure that your git installation is set up to [cache your github password in git](https://help.github.com/articles/caching-your-github-password-in-git/)
4. Install yarn by following the instructions:(https://yarnpkg.com/en/docs/install}

5. Clone rewindJS to your local machine:

`cd /path_to/some_working_directory && git clone https://smlamont@bitbucket.org/smlamont/rewindJS.git`

6. Go into the new rewindJS directory and install the core npm packages using yarn:

`cd rewindJS && yarn install`

Create a new rewindJS-based project
------------------------------------------
1. Open your terminal and go into your local copy of rewindJS (note the path depends on where you originally put rewindJS):

`cd /path_to/where_you_keep_rewindJS`

2. Switch to the master branch, pull the latest changes, and update the node packages:

`git checkout master && git pull && yarn install --force`

3. While in the rewindJS directory, scaffold a new embedded or standalone project:

Embedded app: `gulp scaffold -dir ../name_of_new_app --embedded`

Standalone app: `gulp scaffold -dir ../name_of_new_app --standalone`

Note that you can add a --bare option to the end, if you don't want any sample code in your new project.

4. Go to your project directory and install the node and bower packages:

`cd ../name_of_new_app && yarn install`

5. At this point, you should be able to use gulp tasks to build and run your project:

`gulp run`

6. Now you are ready to build and customize your app. Here are some things you should do:

- Update your readme.md file so other developers know what your project is for and how to use it.
- Update your [package.json file rewindJSconfig options](docs/package_settings.md) so your app uses the right resources and build properties.
- Add your application logic and code to app.html, main.js, and main.scss.
- The scaffolding process creates lots of sample boilerplate code and files inside your src directory. Change and/or delete what you don't need.  

Usage
=====
Check out this [table](docs/libraries_loaded.md) for details on what JS/CSS files are loaded for your project based on rewindJSconfig options.

Standalone apps
---------------
Standalone apps don't run in the city's WordPress site. Instead, they run on their own.
They are ideal for creating internal (intranet) applications. If you are doing a standalone app, you should:

- Make sure that the isEmbedded property in your package.json file is to false
- Use the rewindJS_app class to create your application 'cframe'. Check out [rewindJS_app.js](src/js/rewindJS_app.js) for more documentation.


Creating forms
--------------
If you are doing a web form, use the RewindForm class.
Make sure that your package.json has the core configuration "includeFormValidation" set to true.
Check out rewindJS_forms.js for documentation.

You may also want to use [Backbone](http://backbonejs.org/)-based data modelling.
If so, make sure you have the rewindJS configuration option "includeModeling" set to true.
Also check out cot_backbone.js.

You may want to extend your forms with other package.json options:
* "includeEditableSelect": true, //include editableSelect control
* "includePlaceholders": true, //include placeholders shim
* "includeMultiSelect": true, //include multiselect control
* "includeDatePicker": true, //include date picker control
* "includeRangePicker": true, //include date range picker control
* "includeIntlTelInput": true, //include the intl-tel-input plugin for telephone fields


Other use cases
---------------
There are other rewindJS configuration options you can specify in your package.json to help with common design patterns.

Read more about all the [package.json file rewindJSconfig options](docs/package_settings.md).

Updating the rewindJS Library
===========================
Read about how to update the rewindJS library at [updating_rewindJS](docs/updating_rewindJS.md).
